﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyable_Object : MonoBehaviour {

    public float valueDamage = 10f;
    public float valueSpeed = 10f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector2.left * valueSpeed * Time.deltaTime);
	}

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            // player collides with obstacle!
            //collision.GetComponent<Player_Character>().health -= damage;
            //collision.GetComponent<Player_Character>().value_score -= value_damage;
            //Debug.Log("Player took " + damage + " damage");
            //Debug.Log("Player loses " + value_damage + " points!");


            collision.GetComponent<Player_Character>().value_score -= valueDamage;
            Debug.Log("Obstacle Hit! -" + valueDamage);
            Destroy(gameObject);
        }

        if (collision.CompareTag("Firewall"))
        {
            Debug.Log("Destroyed by Fireall");
            Destroy(gameObject);
        }
    }
}
