﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameHandler : MonoBehaviour
{
    public enum GameState { OPENING, COUNTING, GLIDING, ENDING };
    public GameState state = GameState.OPENING;
    public float gameCountdown = 5f;

    [SerializeField]
    Animator openingAnimator;
    [SerializeField]
    Text playerName;
    [SerializeField]
    Text countDown;
    //private float 
    [System.Serializable]
    public class Glide
    {
        public string playerName;
        public int timeCountdown;
        public float timeSession;
    }

    public Glide[] glides;

    // Start is called before the first frame update
    void Start()
    {
        if (openingAnimator == null)
        {
            Debug.LogError("No animator found");
            this.enabled = false;
        }
        if (playerName == null)
        {
            Debug.LogError("No playerName found");
            this.enabled = false;
        }
        if (countDown == null)
        {
            Debug.LogError("No countDown found");
            this.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case GameState.COUNTING:
                UpdateCountdownUI();
                break;

            case GameState.GLIDING:
                UpdateGlidingUI();
                break;

            case GameState.OPENING:
                UpdateOpeningUI();
                break;

            case GameState.ENDING:
                UpdateEndingUI();
                break;
        }
    }

    /*
    UI UPDATE - COUNTDOWN
    */
    void UpdateCountdownUI()
    {
        Debug.Log("COUNTING");
    }
    /*
    UI UPDATE - GLIDING
    */
    void UpdateGlidingUI()
    {
        Debug.Log("GLIDING");
    }
    /*
    UI UPDATE - OPENING
    */
    void UpdateOpeningUI()
    {
        Debug.Log("OPENING");
    }
    /*
    UI UPDATE - ENDING
    */
    void UpdateEndingUI()
    {
        Debug.Log("ENDING");
    }
}
