﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum valTypes
{
    STATIC,
    FLOATING
};

class SpawnerTypes
{
    int typeStatic = 0;
    int typeFloating = 1;
}

public class Spawner : MonoBehaviour {

    public GameObject obstacle;
    public float startTimeBtwSpawn;
   

    string[] spawnerType = { "static", "floating" };
    int typePointer = (System.Enum.GetValues(typeof(valTypes)).Length);
    List<SpawnerTypes> listTypes = new List<SpawnerTypes>();

    //private Quaternion spawnedRotation;
    private float timeBtwSpawn;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if (timeBtwSpawn <= 0)
        {
            //spawnedRotation = new Quaternion.Euler(); 
            // Attempting to rotate the spawned child to face the character
            Instantiate(obstacle, transform.position, Quaternion.Euler(0, 0, 0));
            timeBtwSpawn = startTimeBtwSpawn;
        }
        else
        {
            timeBtwSpawn -= Time.deltaTime;
        }

	}
}
