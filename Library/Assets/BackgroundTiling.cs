﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundTiling : MonoBehaviour
{

    public Transform[] backgrounds;         // Array (list) of all the back and foregrounds to be parallaxed
    private float[] parallaxScales;         // The proportion of the camera's movement to move the backgrounds by
    private float[] frameLengths;
    private float[] frameStartPos;
    public float smoothing = 1f;
    public float distanceLead = 1f;
    private float length, startpos;
    public GameObject mainCam;
    public float parallaxEffect;
    private Transform cam;                  // refeence to the main transform
    
    // is called before Start(). Great for references
    private void Awake()
    {
        cam = Camera.main.transform;
    }


    // Start is called before the first frame update
    void Start()
    {
        // setting the proper array sizes
        parallaxScales = new float[backgrounds.Length];
        frameLengths = new float[backgrounds.Length];
        frameStartPos = new float[backgrounds.Length];
        for (int i=0; i < backgrounds.Length; i++)
        {
            parallaxScales[i] = backgrounds[i].position.z * -1;
            frameLengths[i] = backgrounds[i].GetComponent<SpriteRenderer>().bounds.size.x;
            frameStartPos[i] = backgrounds[i].transform.position.x;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        
    }
}
