﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Consumable : MonoBehaviour
{

    public float valueBonus = 5f;
    public float valueSpeed = 10f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.left * valueSpeed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            // player collides with obstacle!
            collision.GetComponent<Player_Character>().value_score += valueBonus;
            Debug.Log("Obstacle Hit! -" + valueBonus);
            Destroy(gameObject);
        }

        if (collision.CompareTag("Firewall"))
        {
            Debug.Log("Destroyed by Fireall");
            Destroy(gameObject);
        }
    }
}
