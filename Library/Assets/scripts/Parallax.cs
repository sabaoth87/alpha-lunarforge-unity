﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{

    public Transform[] backgrounds;     // Array (list) of all the back and foregrounds to be parallaxed
    private float[] parallaxScales;         // The proportion of the camera's movement to move the backgrounds by
    private float[] frameLengths;
    private float[] frameStartPos;
    public float smoothing = 1f;
    public float distanceLead = 1f;
    private float length, startpos;
    public GameObject mainCam;
    public float parallaxEffect;

    private Transform cam;                  // refeence to the main transform
    private Vector3 previousCamPos;        // the position of the previous frame

    // is called before Start(). Great for references
    private void Awake()
    {
        cam = Camera.main.transform;
    }

    // Start is called before the first frame update
    void Start()
    {
        // The previous frame had the current frame's camera position
        previousCamPos = cam.position;
        //startpos = transform.position.x;
        //length = GetComponent<SpriteRenderer>().bounds.size.x;

        // setting the proper array sizes
        parallaxScales = new float[backgrounds.Length];
        frameLengths = new float[backgrounds.Length];
        frameStartPos = new float[backgrounds.Length];

        for (int i=0; i < backgrounds.Length; i++)
        {
            parallaxScales[i] = backgrounds[i].position.z * -1;
            frameLengths[i] = backgrounds[i].GetComponent<SpriteRenderer>().bounds.size.x;
            frameStartPos[i] = backgrounds[i].transform.position.x;
        }

    }

    //
    private void FixedUpdate()
    {
        float temp = (cam.transform.position.x * (1 - parallaxEffect));
        float distance = (cam.transform.position.x * parallaxEffect);

        for (int i=0; i < backgrounds.Length; i++)
        {
            if (temp > frameStartPos[i] + frameLengths[i]) frameStartPos[i] += frameLengths[i] + (Screen.width *2);
            //if (temp > frameStartPos[i] + frameLengths[i]) frameStartPos[i] += cam.transform.position.x;
            else if (temp < frameStartPos[i] - frameLengths[i]) frameStartPos[i] -= frameLengths[i];
            backgrounds[i].position = new Vector3(((frameStartPos[i] + distance)), backgrounds[i].position.y, backgrounds[i].position.z);

        }
        //transform.position = new Vector3(startpos + distance, transform.position.y, transform.position.z);

        //for (int i=0; i < frameLengths.Length; i++){
        //    if (temp > startpos + length) startpos += length;
        //    else if (temp < startpos - length) startpos -= length;
        //}
        
    }

    // Update is called once per frame
    void Update()
    {
        // for each background
        for (int i=0; i < backgrounds.Length; i++)
        {
            // the parallax is the opposite of the camera movement because the previous frame is multiplied by the scale
            float parallax = (previousCamPos.x - cam.position.x) * parallaxScales[i];

            //set a target x position which is the current position plus the parallax
            float backgroundTargetPosX = backgrounds[i].position.x + parallax;

            // create a target position which is the background's current position with it's target x position
            Vector3 backgroundTargetPos = new Vector3(backgroundTargetPosX, backgrounds[i].position.y, backgrounds[i].position.z);

            // fade between current position and target position using lerp
            backgrounds[i].position = Vector3.Lerp(backgrounds[i].position, backgroundTargetPos, smoothing * Time.deltaTime);
        }

        // set the previousCamPos to the camera's position at the end of the frame
        previousCamPos = cam.position;
    }
}
